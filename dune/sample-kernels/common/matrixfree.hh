#ifndef DUNE_MATRIXFREE_HH
#define DUNE_MATRIXFREE_HH

#include <iostream>

#include <dune/pdelab/backend/istl.hh>

#include "matrixfreeistlbackends.hh"

namespace Dune{
  namespace PDELab{

    namespace impl{
      template <typename LS, typename GO, typename V>
      void solveMatrixFree(LS& ls, GO& go, V &x){
        Dune::InverseOperatorResult stat;
        // evaluate residual w.r.t initial guess
        using TrialGridFunctionSpace = typename GO::Traits::TrialGridFunctionSpace;
        using W = Dune::PDELab::Backend::Vector<TrialGridFunctionSpace,typename V::ElementType>;
        W r(go.testGridFunctionSpace(),0.0);
        go.residual(x,r);
        // solve the jacobian system
        V z(go.trialGridFunctionSpace(),0.0);
        ls.apply(z,r,stat);
        x -= z;
      }

    }


    template <typename GO, typename V>
    void solveMatrixFreeBiCGSTAB(GO &go, V &x){
      using ISTLOnTheFlyOperator = Dune::PDELab::OnTheFlyOperator<V,V,GO>;
      ISTLOnTheFlyOperator opb(go);
      Dune::Richardson<V,V> richardson(1.0);
      Dune::BiCGSTABSolver<V> solverb(opb,richardson,1E-10,5000,2);
      impl::solveMatrixFree(solverb, go, x);
    }

    template <typename GO, typename V>
    void solveMatrixFreeMINRES(GO &go, V &x){
      using ISTLOnTheFlyOperator = Dune::PDELab::OnTheFlyOperator<V,V,GO>;
      ISTLOnTheFlyOperator opb(go);
      Dune::Richardson<V,V> richardson(1.0);
      Dune::MINRESSolver<V> solverb(opb,richardson,1E-10,5000,2);
      impl::solveMatrixFree(solverb, go, x);
    }
  } // namespace PDELab
} // namespace Dune

#endif
