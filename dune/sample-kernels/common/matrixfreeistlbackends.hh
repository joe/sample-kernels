#ifndef DUNE_MATRIXFREEISTLBACKENDS_HH
#define DUNE_MATRIXFREEISTLBACKENDS_HH

#include <dune/istl/operators.hh>

namespace Dune {
  namespace PDELab {


    /** \brief Application of jacobian from nonlinear problems.

        \tparam X  Trial vector.
        \tparam Y  Test vector.
        \tparam GO Grid operator implementing the operator application.
    */
    template<typename X, typename Y, typename GO>
    class NonlinearOnTheFlyOperator : public Dune::LinearOperator<X,Y>
    {
    public :
      typedef X domain_type;
      typedef Y range_type;
      typedef typename X::field_type field_type;

      NonlinearOnTheFlyOperator(const GO& go)
        : go_(go)
        , u_(static_cast<X*>(0))
      {}

      //! Set position of jacobian. This is different to linear problems.
      //! Must be called before both apply() and applyscaleadd().
      void setLinearizationPoint(const X& u)
      {
        u_ = &u;
      }

      virtual void apply(const X& x, Y& y) const
      {
        y = 0.0;
        go_.nonlinear_jacobian_apply(*u_,x,y);
      }

      virtual void applyscaleadd(field_type alpha, const X& x, Y& y) const
      {
        Y temp(y);
        temp = 0.0;
        go_.nonlinear_jacobian_apply(*u_,x,temp);
        y.axpy(alpha,temp);
      }

      virtual Dune::SolverCategory::Category category() const
      {
        return Dune::SolverCategory::sequential;
      }

    private :
      const GO& go_;
      const X* u_;
    };



    template<template<class> class Solver>
    class ISTLBackend_SEQ_Richardson
      : public SequentialNorm, public LinearResultStorage
    {
    public:
      /*! \brief make a linear solver object

        \param[in] maxiter_ maximum number of iterations to do
        \param[in] verbose_ print messages if true
      */
      explicit ISTLBackend_SEQ_Richardson(unsigned maxiter_=5000, int verbose_=1)
        : maxiter(maxiter_), verbose(verbose_)
      {}



      /*! \brief solve the given linear system

        \param[in] A the given matrix
        \param[out] z the solution vector to be computed
        \param[in] r right hand side
        \param[in] reduction to be achieved
      */
      template<class M, class V, class W>
      void apply(M& A, V& z, W& r, typename Dune::template FieldTraits<typename W::ElementType >::real_type reduction)
      {
        using Backend::Native;
        using Backend::native;

        Dune::MatrixAdapter<Native<M>,
                            Native<V>,
                            Native<W>> opa(native(A));
        Dune::Richardson<Native<V>,Native<W> > prec(0.7);
        Solver<Native<V> > solver(opa, prec, reduction, maxiter, verbose);
        Dune::InverseOperatorResult stat;
        solver.apply(native(z), native(r), stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
      }

    private:
      unsigned maxiter;
      int verbose;
    };


    template<class Operator, template<class> class Solver>
    class ISTLBackend_SEQ_MatrixFree_Richardson
      : public SequentialNorm, public LinearResultStorage
    {
    public:
      /*! \brief make a linear solver object

        \param[in] maxiter_ maximum number of iterations to do
        \param[in] verbose_ print messages if true
      */
      explicit ISTLBackend_SEQ_MatrixFree_Richardson(Operator& op, unsigned maxiter=5000, int verbose=1)
        : opa_(op), u_(static_cast<typename Operator::domain_type*>(0))
        , maxiter_(maxiter)
        , verbose_(verbose)
      {}



      /*! \brief solve the given linear system

        \param[in] A the given matrix
        \param[out] z the solution vector to be computed
        \param[in] r right hand side
        \param[in] reduction to be achieved
      */
      template<class V, class W>
      void apply(V& z, W& r, typename Dune::template FieldTraits<typename W::ElementType >::real_type reduction)
      {
        Dune::Richardson<V,W> prec(0.7);
        Solver<V> solver(opa_, prec, reduction, maxiter_, verbose_);
        Dune::InverseOperatorResult stat;
        solver.apply(z, r, stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
      }

      //! Set position of jacobian.
      //! Must be called before apply().
      void setLinearizationPoint(const typename Operator::domain_type& u)
      {
        u_ = &u;
        opa_.setLinearizationPoint(u);
      }

    private:
      Operator& opa_;
      const typename Operator::domain_type* u_;
      unsigned maxiter_;
      int verbose_;
    };

  } // end namespace PDELab
} // end namespace Dune
#endif
