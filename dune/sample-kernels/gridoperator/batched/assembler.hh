#ifndef DUNE_PDELAB_GRIDOPERATOR_BATCHED_ASSEMBLER_HH
#define DUNE_PDELAB_GRIDOPERATOR_BATCHED_ASSEMBLER_HH

#include <vector>

#include <dune/common/typetraits.hh>
#include <dune/pdelab/gridoperator/common/assemblerutilities.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include <dune/pdelab/common/geometrywrapper.hh>
#include <dune/pdelab/common/intersectiontype.hh>

namespace Dune{
  namespace PDELab{

    /**
       \brief The assembler for standard DUNE grid

       * \tparam GFSU GridFunctionSpace for ansatz functions
       * \tparam GFSV GridFunctionSpace for test functions
       */

    template<typename GFSU, typename GFSV, typename CU, typename CV>
    class BatchedAssembler {
    public:

      //! Types related to current grid view
      //! @{
      using EntitySet = typename GFSU::Traits::EntitySet;
      using Element = typename EntitySet::Element;
      using Intersection = typename EntitySet::Intersection;
      //! @}

      //! Grid function spaces
      //! @{
      typedef GFSU TrialGridFunctionSpace;
      typedef GFSV TestGridFunctionSpace;
      //! @}

      //! Size type as used in grid function space
      typedef typename GFSU::Traits::SizeType SizeType;

      //! Static check on whether this is a Galerkin method
      static const bool isGalerkinMethod = std::is_same<GFSU,GFSV>::value;

      BatchedAssembler (const std::size_t batch_size_, const GFSU& gfsu_, const GFSV& gfsv_, const CU& cu_, const CV& cv_)
        : gfsu(gfsu_)
        , gfsv(gfsv_)
        , cu(cu_)
        , cv(cv_)
        , batch_size(batch_size_)
      { }

      BatchedAssembler (const std::size_t batch_size_, const GFSU& gfsu_, const GFSV& gfsv_)
        : gfsu(gfsu_)
        , gfsv(gfsv_)
        , cu()
        , cv()
        , batch_size(batch_size_)
      { }

      //! Get the trial grid function space
      const GFSU& trialGridFunctionSpace() const
      {
        return gfsu;
      }

      //! Get the test grid function space
      const GFSV& testGridFunctionSpace() const
      {
        return gfsv;
      }

      // Assembler (const GFSU& gfsu_, const GFSV& gfsv_)
      //   : gfsu(gfsu_), gfsv(gfsv_), lfsu(gfsu_), lfsv(gfsv_),
      //     lfsun(gfsu_), lfsvn(gfsv_),
      //     sub_triangulation(ST(gfsu_.gridview(),Dune::PDELab::NoSubTriangulationImp()))
      // { }

      template<class LocalAssemblerEngine>
      void assemble(LocalAssemblerEngine & assembler_engine) const
      {
        using LFSU = typename LocalAssemblerEngine::LFSU;
        using LFSV = typename LocalAssemblerEngine::LFSV;

        using LFSUCache = typename LocalAssemblerEngine::LFSUCache;
        using LFSVCache = typename LocalAssemblerEngine::LFSVCache;

        const bool needs_constraints_caching = assembler_engine.needsConstraintsCaching(cu,cv);

        // Notify assembler engine about oncoming assembly
        assembler_engine.preAssembly();

        auto entity_set = gfsu.entitySet();
        //auto& index_set = entity_set.indexSet();

        std::vector<ElementGeometry<Element>> eg_batch;
        std::vector<LFSU> lfsu_batch;
        std::vector<LFSV> lfsv_batch;
        std::vector<LFSUCache> lfsu_cache_batch;
        std::vector<LFSVCache> lfsv_cache_batch;

        eg_batch.reserve(batch_size);
        lfsu_batch.reserve(batch_size);
        lfsv_batch.reserve(batch_size);
        lfsu_cache_batch.reserve(batch_size);
        lfsv_cache_batch.reserve(batch_size);

        for (std::size_t i = 0; i < batch_size; ++i) {
          lfsu_batch.emplace_back(gfsu);
          lfsv_batch.emplace_back(gfsv);

          lfsu_cache_batch.emplace_back(lfsu_batch[i], cu, needs_constraints_caching);
          lfsv_cache_batch.emplace_back(lfsv_batch[i], cv, needs_constraints_caching);
        }

        auto bind_batch = [&] () {
          // Notify assembler engine about bind
          assembler_engine.onBindLFSV(eg_batch, lfsv_cache_batch);

          // Notify assembler engine about bind
          assembler_engine.onBindLFSUV(eg_batch, lfsu_cache_batch, lfsv_cache_batch);

          // Load coefficients of local functions
          assembler_engine.loadCoefficientsLFSUInside(eg_batch, lfsu_cache_batch);
        };

        auto execute_batch = [&] () {
            // Volume integration
            assembler_engine.assembleUVVolume(eg_batch, lfsu_cache_batch, lfsv_cache_batch);
        };

        auto unbind_batch = [&] () {
          // Notify assembler engine about bind
          assembler_engine.onUnbindLFSV(eg_batch, lfsv_cache_batch);

          // Notify assembler engine about bind
          assembler_engine.onUnbindLFSUV(eg_batch, lfsu_cache_batch, lfsv_cache_batch);
        };


        // Traverse grid view
        for (const auto& element : elements(entity_set))
          {
            // Compute unique id
            //auto ids = index_set.uniqueIndex(element);

            ElementGeometry<Element> eg(element);

            if(assembler_engine.assembleCell(eg))
              continue;

            if(eg_batch.size() == batch_size){
              bind_batch();

              execute_batch();

              unbind_batch();

              eg_batch.clear();
            }

            eg_batch.push_back(eg);

            const auto i = eg_batch.size() - 1;

            // Bind local test function space to element
            lfsv_batch[i].bind( element );
            lfsv_cache_batch[i].update();

            // Bind local trial function space to element
            lfsu_batch[i].bind( element );
            lfsu_cache_batch[i].update();
          } // it

          if(eg_batch.size() > 0){
            bind_batch();

            execute_batch();

            unbind_batch();
          }

        // Notify assembler engine that assembly is finished
        assembler_engine.postAssembly(gfsu,gfsv);

      }

    private:

      /* global function spaces */
      const GFSU& gfsu;
      const GFSV& gfsv;

      typename std::conditional<
        std::is_same<CU,EmptyTransformation>::value,
        const CU,
        const CU&
        >::type cu;
      typename std::conditional<
        std::is_same<CV,EmptyTransformation>::value,
        const CV,
        const CV&
        >::type cv;

      std::size_t batch_size;

    };

  }
}
#endif // DUNE_PDELAB_GRIDOPERATOR_BATCHED_ASSEMBLER_HH
