#ifndef DUNE_PDELAB_GRIDOPERATOR_BATCHED_JACOBIANAPPLYENGINE_HH
#define DUNE_PDELAB_GRIDOPERATOR_BATCHED_JACOBIANAPPLYENGINE_HH

#include <dune/pdelab/gridfunctionspace/localvector.hh>
#include <dune/pdelab/gridoperator/common/assemblerutilities.hh>
#include <dune/pdelab/gridoperator/common/localassemblerenginebase.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/localoperator/callswitch.hh>

namespace Dune{
  namespace PDELab{

    /**
       \brief The local assembler engine for DUNE grids which
       assembles the local application of the Jacobian

       \tparam LA The local assembler

    */
    template<typename LA>
    class BatchedLocalJacobianApplyAssemblerEngine
      : public LocalAssemblerEngineBase
    {
    public:

      template<typename TrialConstraintsContainer, typename TestConstraintsContainer>
      bool needsConstraintsCaching(const TrialConstraintsContainer& cu, const TestConstraintsContainer& cv) const
      {
        return false;
      }

      //! The type of the wrapping local assembler
      typedef LA LocalAssembler;

      //! The type of the local operator
      typedef typename LA::LocalOperator LOP;

      //! The type of the residual vector
      typedef typename LA::Traits::Residual Residual;
      typedef typename Residual::ElementType ResidualElement;

      //! The type of the solution vector
      typedef typename LA::Traits::Solution Solution;
      typedef typename Solution::ElementType SolutionElement;

      //! The local function spaces
      typedef typename LA::LFSU LFSU;
      typedef typename LA::LFSUCache LFSUCache;
      typedef typename LFSU::Traits::GridFunctionSpace GFSU;
      typedef typename LA::LFSV LFSV;
      typedef typename LA::LFSVCache LFSVCache;
      typedef typename LFSV::Traits::GridFunctionSpace GFSV;

      typedef typename Solution::template ConstLocalView<LFSUCache> SolutionView;
      typedef typename Residual::template LocalView<LFSVCache> ResidualView;

      /**
         \brief Constructor

         \param [in] local_assembler_ The local assembler object which
         creates this engine
      */
      BatchedLocalJacobianApplyAssemblerEngine(const std::size_t batch_size_, const LocalAssembler & local_assembler_)
        : batch_size(batch_size_),
          local_assembler(local_assembler_),
          lop(local_assembler_.localOperator()),
          global_rl_view(batch_size_),
          global_sl_view(batch_size_),
          xl(batch_size_),
          rl(batch_size_)
      {
        rl_view.reserve(batch_size_);
        for (std::size_t i = 0; i < batch_size_; ++i)
          rl_view.emplace_back(rl[i], 1.0);
      }

      //! Public access to the wrapping local assembler
      const LocalAssembler & localAssembler() const
      {
        return local_assembler;
      }

      //! Trial space constraints
      const typename LocalAssembler::Traits::TrialGridFunctionSpaceConstraints& trialConstraints() const
      {
        return localAssembler().trialConstraints();
      }

      //! Test space constraints
      const typename LocalAssembler::Traits::TestGridFunctionSpaceConstraints& testConstraints() const
      {
        return localAssembler().testConstraints();
      }

      //! Set current residual vector. Should be called prior to
      //! assembling.
      void setResidual(Residual & residual_)
      {
        for (std::size_t i = 0; i < global_rl_view.size(); ++i)
          global_rl_view[i].attach(residual_);
      }

      //! Set current solution vector. Should be called prior to
      //! assembling.
      void setSolution(const Solution & solution_)
      {
        for (std::size_t i = 0; i < global_sl_view.size(); ++i)
          global_sl_view[i].attach(solution_);
      }

      //! Called immediately after binding of local function space in
      //! global assembler.
      //! @{
      template<typename EG, typename LFSUC, typename LFSVC>
      void onBindLFSUV(const EG & eg, const LFSUC & lfsu_cache, const LFSVC & lfsv_cache)
      {
        for (std::size_t i = 0; i < eg.size(); ++i) {
          global_sl_view[i].bind(lfsu_cache[i]);
          xl[i].resize(lfsu_cache[i].size());
        }
      }

      template<typename EG, typename LFSVC>
      void onBindLFSV(const EG & eg, const LFSVC & lfsv_cache)
      {
        for (std::size_t i = 0; i < eg.size(); ++i) {
          global_rl_view[i].bind(lfsv_cache[i]);
          rl[i].assign(lfsv_cache[i].size(), 0.0);
        }
      }

      //! @}

      //! Called when the local function space is about to be rebound or
      //! discarded
      //! @{
      template<typename EG, typename LFSVC>
      void onUnbindLFSV(const EG & eg, const LFSVC & lfsv_cache)
      {
        for (std::size_t i = 0; i < eg.size(); ++i) {
          global_rl_view[i].add(rl[i]);
          global_rl_view[i].commit();
        }
      }

      //! Methods for loading of the local function's coefficients
      //! @{
      template<typename EG, typename LFSUC>
      void loadCoefficientsLFSUInside(const EG & eg, const LFSUC & lfsu_s_cache)
      {
        for (std::size_t i = 0; i < eg.size(); ++i)
          global_sl_view[i].read(xl[i]);
      }
      //! @}

      //! Notifier functions, called immediately before and after assembling
      //! @{

      void postAssembly(const GFSU& gfsu, const GFSV& gfsv)
      {
        if(local_assembler.doPostProcessing())
          for (std::size_t i = 0; i < global_rl_view.size(); ++i)
            Dune::PDELab::constrain_residual(local_assembler.testConstraints(),global_rl_view[i].container());

      }

      //! @}

      //! Assembling methods
      //! @{

      /** Assemble on a given cell without function spaces.

          \return If true, the assembling for this cell is assumed to
          be complete and the assembler continues with the next grid
          cell.
       */
      template<typename EG>
      bool assembleCell(const EG & eg)
      {
        return LocalAssembler::isNonOverlapping && eg.entity().partitionType() != Dune::InteriorEntity;
      }

      template<typename EG, typename LFSUC, typename LFSVC>
      void assembleUVVolume(const EG & eg, const LFSUC & lfsu_cache, const LFSVC & lfsv_cache)
      {
        for (std::size_t i = 0; i < eg.size(); ++i) {
          rl_view[i].setWeight(local_assembler.weight());
        }
        Dune::PDELab::LocalAssemblerCallSwitch<LOP,LOP::doAlphaVolume>::
          jacobian_apply_volume(lop,eg,lfsu_cache,xl,lfsv_cache,rl_view);
      }

      //! @}

    private:
      std::size_t batch_size;

      //! Reference to the wrapping local assembler object which
      //! constructed this engine
      const LocalAssembler & local_assembler;

      //! Reference to the local operator
      const LOP & lop;

      //! Pointer to the current residual vector in which to assemble
      std::vector<ResidualView> global_rl_view;

      //! Pointer to the current residual vector in which to assemble
      std::vector<SolutionView> global_sl_view;

      //! The local vectors and matrices as required for assembling
      //! @{
      typedef Dune::PDELab::TrialSpaceTag LocalTrialSpaceTag;
      typedef Dune::PDELab::TestSpaceTag LocalTestSpaceTag;

      typedef Dune::PDELab::LocalVector<SolutionElement, LocalTrialSpaceTag> SolutionVector;
      typedef Dune::PDELab::LocalVector<ResidualElement, LocalTestSpaceTag> ResidualVector;

      //! Inside local coefficients
      std::vector<SolutionVector> xl;
      //! Inside local residual
      std::vector<ResidualVector> rl;
      //! Inside local residual weighted view
      std::vector<typename ResidualVector::WeightedAccumulationView> rl_view;
      //! @}

    }; // End of class BatchedLocalJacobianAssemblerEngine

  }
}
#endif // DUNE_PDELAB_GRIDOPERATOR_BATCHED_JACOBIANAPPLYENGINE_HH
