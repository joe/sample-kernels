#ifndef DUNE_PDELAB_GRIDOPERATOR_BATCHED_GRIDOPERATOR_HH
#define DUNE_PDELAB_GRIDOPERATOR_BATCHED_GRIDOPERATOR_HH

#include <tuple>

#include <dune/common/hybridutilities.hh>

#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridoperator/common/borderdofexchanger.hh>
#include <dune/pdelab/gridoperator/common/gridoperatorutilities.hh>
#include <dune/sample-kernels/gridoperator/batched/assembler.hh>
#include <dune/sample-kernels/gridoperator/batched/localassembler.hh>

namespace Dune{
  namespace PDELab{

    /**
       \brief Standard grid operator implementation

       \tparam GFSU GridFunctionSpace for ansatz functions
       \tparam GFSV GridFunctionSpace for test functions
       \tparam MB The matrix backend to be used for representation of the jacobian
       \tparam DF The domain field type of the operator
       \tparam RF The range field type of the operator
       \tparam JF The jacobian field type
       \tparam CU   Constraints maps for the individual dofs (trial space)
       \tparam CV   Constraints maps for the individual dofs (test space)

    */
    template<typename GFSU, typename GFSV, typename LOP,
        typename MB, typename DF, typename RF, typename JF,
        typename CU=Dune::PDELab::EmptyTransformation,
        typename CV=Dune::PDELab::EmptyTransformation
    >
    class BatchedGridOperator
    {
    public:

      //! The global assembler type
      typedef BatchedAssembler<GFSU,GFSV,CU,CV> Assembler;

      //! The type of the domain (solution).
      using Domain = Dune::PDELab::Backend::Vector<GFSU,DF>;
      //! The type of the range (residual).
      using Range = Dune::PDELab::Backend::Vector<GFSV,RF>;
      //! The type of the jacobian.
      using Jacobian = Dune::PDELab::Backend::Matrix<MB,Domain,Range,JF>;

      //! The sparsity pattern container for the jacobian matrix
      typedef typename MB::template Pattern<Jacobian,GFSV,GFSU> Pattern;

      //! The local assembler type
      typedef BatchedLocalAssembler<
          BatchedGridOperator,
          LOP,
          GFSU::Traits::EntitySet::Partitions::partitionIterator() == InteriorBorder_Partition
      > LocalAssembler;

      // Fix this as soon as the default Partitions are constexpr
      typedef typename std::conditional<
          GFSU::Traits::EntitySet::Partitions::partitionIterator() == InteriorBorder_Partition,
          NonOverlappingBorderDOFExchanger<BatchedGridOperator>,
          OverlappingBorderDOFExchanger<BatchedGridOperator>
      >::type BorderDOFExchanger;

      //! The grid operator traits
      typedef Dune::PDELab::GridOperatorTraits
          <GFSU,GFSV,MB,DF,RF,JF,CU,CV,Assembler,LocalAssembler> Traits;

      template <typename MFT>
      struct MatrixContainer{
        typedef typename Traits::Jacobian Type;
      };

      //! Constructor for non trivial constraints
      BatchedGridOperator(const std::size_t batch_size, const GFSU & gfsu_, const CU & cu_,
                          const GFSV & gfsv_, const CV & cv_, LOP & lop_, const MB& mb_ = MB())
          : global_assembler(batch_size, gfsu_,gfsv_,cu_,cv_)
          , dof_exchanger(std::make_shared<BorderDOFExchanger>(*this))
          , local_assembler(batch_size, lop_, cu_, cv_,dof_exchanger)
          , backend(mb_)
      {}

      //! Constructor for empty constraints
      BatchedGridOperator(const std::size_t batch_size, const GFSU & gfsu_, const GFSV & gfsv_,
                          LOP & lop_, const MB& mb_ = MB())
          : global_assembler(batch_size, gfsu_,gfsv_)
          , dof_exchanger(std::make_shared<BorderDOFExchanger>(*this))
          , local_assembler(batch_size, lop_,dof_exchanger)
          , backend(mb_)
      {}

      //! Get the trial grid function space
      const GFSU& trialGridFunctionSpace() const
      {
        return global_assembler.trialGridFunctionSpace();
      }

      //! Get the test grid function space
      const GFSV& testGridFunctionSpace() const
      {
        return global_assembler.testGridFunctionSpace();
      }

      //! Get dimension of space u
      typename GFSU::Traits::SizeType globalSizeU () const
      {
        return trialGridFunctionSpace().globalSize();
      }

      //! Get dimension of space v
      typename GFSV::Traits::SizeType globalSizeV () const
      {
        return testGridFunctionSpace().globalSize();
      }

      Assembler & assembler() { return global_assembler; }

      const Assembler & assembler() const { return global_assembler; }

      LocalAssembler & localAssembler() const { return local_assembler; }


      //! Visitor which is called in the method setupGridOperators for
      //! each tuple element.
      template <typename GridOperatorTuple>
      struct SetupGridOperator
      {
        SetupGridOperator()
            : index(0), size(std::tuple_size<GridOperatorTuple>::value) {}

        template <typename T>
        void visit(T& elem) {
          elem.localAssembler().preProcessing(index == 0);
          elem.localAssembler().postProcessing(index == size-1);
          ++index;
        }

        int index;
        const int size;
      };

      //! Method to set up a number of grid operators which are used
      //! in a joint assembling. It is assumed that all operators are
      //! specializations of the same template type
      template<typename GridOperatorTuple>
      static void setupGridOperators(GridOperatorTuple tuple)
      {
        SetupGridOperator<GridOperatorTuple> setup_visitor;
        Hybrid::forEach(tuple,
                        [&](auto &el) { setup_visitor.visit(el); });
      }

      //! Assemble residual
      void residual(const Domain & x, Range & r) const
      {
        typedef typename LocalAssembler::LocalResidualAssemblerEngine ResidualEngine;
        ResidualEngine & residual_engine = local_assembler.localResidualAssemblerEngine(r,x);
        global_assembler.assemble(residual_engine);
      }

      //! Apply jacobian matrix without explicitly assembling it
      void jacobian_apply(const Domain & z, Range & r) const
      {
        typedef typename LocalAssembler::LocalJacobianApplyAssemblerEngine JacobianApplyEngine;
        JacobianApplyEngine & jacobian_apply_engine = local_assembler.localJacobianApplyAssemblerEngine(r,z);
        global_assembler.assemble(jacobian_apply_engine);
      }

    private:
      Assembler global_assembler;
      shared_ptr<BorderDOFExchanger> dof_exchanger;

      mutable LocalAssembler local_assembler;
      MB backend;

    };

  }
}
#endif // DUNE_PDELAB_GRIDOPERATOR_BATCHED_GRIDOPERATOR_HH
