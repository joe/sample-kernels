// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef SAMPLE_KERNELS_BATCHED_LOP_HH
#define SAMPLE_KERNELS_BATCHED_LOP_HH


#include "dune/localfunctions/lagrange/qk/qklocalbasis.hh"
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/pdelab/finiteelement/localbasiscache.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/pdelab/localoperator/defaultimp.hh"

template<typename LOP>
struct BatchedLocalOperator : public Dune::PDELab::LocalOperatorDefaultFlags
{  
  explicit BatchedLocalOperator(const LOP& lop_) : lop(lop_) {}
   
  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg_batch, const LFSU &lfsu_batch, const X &x_batch,
                    const LFSV &lfsv_batch, R &r_batch) const {  
    for (int l = 0; l < eg_batch.size(); ++l) {
      lop.alpha_volume(eg_batch[l], lfsu_batch[l].localFunctionSpace(), x_batch[l],
                       lfsv_batch[l].localFunctionSpace(), r_batch[l]);
    }
  }
    
  template<typename Y, typename X, typename EG, typename LFSU, typename LFSV>
  void jacobian_apply_volume(const EG &eg_batch, const LFSU &lfsu_batch, const X &x_batch,
                             const LFSV &lfsv_batch, Y &y_batch) const {  
    for (int l = 0; l < eg_batch.size(); ++l) {
      lop.jacobian_apply_volume(eg_batch[l], lfsu_batch[l].localFunctionSpace(), x_batch[l],
                                lfsv_batch[l].localFunctionSpace(), y_batch[l]);
    }
  }
private:
  const LOP& lop;
};

#endif //SAMPLE_KERNELS_BATCHED_LOP_HH
