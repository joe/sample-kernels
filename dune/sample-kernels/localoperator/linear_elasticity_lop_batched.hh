// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_BATCHED_HH
#define SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_BATCHED_HH

#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/typetree/childextraction.hh"
#include "dune/localfunctions/lagrange/qk/qklocalbasis.hh"
#include "dune/pdelab/common/quadraturerules.hh"


// ADD PACXX
#include <PACXX.h>
#include <math.h>



template<typename GFSU, typename GFSV, typename RF, int k>
struct rOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern {

  enum { doPatternVolume = true };
  enum { doAlphaVolume = true };


  using GFSU_0 = Dune::TypeTree::Child<GFSU, 0>;
  using Q1_LocalBasis = Dune::QkLocalBasis<typename GFSU_0::Traits::GridView::ctype, double, 1, 3>;
  Dune::PDELab::LocalBasisCache<Q1_LocalBasis> cache_Q1;
  using JacobianType = typename Q1_LocalBasis::Traits::JacobianType;
  const Q1_LocalBasis Q1_microElementBasis;

  typename GFSU::Traits::GridView::template Codim<0>::Geometry::JacobianInverseTransposed jit;
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::ctype detjac;

  pacxx::v2::Executor* exec;

  std::vector<std::array<Dune::FieldMatrix<double,1,3>, 8> > js_Q1;
  std::vector<double> factors;

  int qp_size;

  rOperator(const GFSU &gfsu, const GFSV &gfsv) :
      Q1_microElementBasis() {
    auto cell_geo = gfsu.gridView().template begin<0>()->geometry();
    const auto quadrature_rule = Dune::PDELab::quadratureRule(cell_geo, 2);

   // qp = quadrature_rule.begin(); // just assume a vector???
    qp_size = quadrature_rule.size();

    jit = cell_geo.jacobianInverseTransposed(Dune::FieldVector<RF, 3>());
    detjac = cell_geo.integrationElement(Dune::FieldVector<RF, 3>());

    for (const auto &qp: quadrature_rule) {
      cache_Q1.evaluateJacobian(qp.position(), Q1_microElementBasis);
    }

    exec = &pacxx::v2::Executor::get(0);


    js_Q1.reserve(qp_size);
    factors.reserve(qp_size);
     for (auto qp : quadrature_rule) {

        const auto & tmp = cache_Q1.evaluateJacobian(qp.position(), Q1_microElementBasis);
        js_Q1.emplace_back();
        std::copy(std::begin(tmp), std::end(tmp), std::begin(js_Q1.back()));

        factors.push_back(qp.weight() * detjac / double(k * k * k));
     }


  }


  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg_batch, const LFSU &lfsu_cache_batch, const X &x_batch,
                    const LFSV &lfsv_cache_batch, R &r_batch) const {

      auto cell_geo = eg_batch[0].entity().geometry();   // should depend on eg_batch and l

      const auto& lfsu = lfsu_cache_batch[0].localFunctionSpace(); // should depend on eg_batch and l
      const auto& lfsv = lfsv_cache_batch[0].localFunctionSpace(); // should depend on eg_batch and l
      using namespace Dune::Indices;
      auto lfsu_0 = Dune::TypeTree::child(lfsu, _0);
      auto lfsu_1 = Dune::TypeTree::child(lfsu, _1);
      auto lfsu_2 = Dune::TypeTree::child(lfsu, _2);

      size_t batch_size = eg_batch.size();


   auto kernel = [&](pacxx::v2::range &config)
    {

     int l = config.get_block(0);

     for (int m = 0; m < qp_size; ++m) 
     {
        JacobianType grad[8] = {};
        for (int i = 0; i < 8; ++i)
          jit.usmv(k, js_Q1[m][i][0], grad[i][0]);

        for (int subel_z = 0; subel_z < k; ++subel_z)
          for (int subel_y = 0; subel_y < k; ++subel_y)
            for (int subel_x = 0; subel_x < k; ++subel_x) {

              double grad_u[3][3] = {};

              for (int idim0 = 0; idim0 <= 2; ++idim0) {
                for (int iz = 0; iz <= 1; ++iz)
                  for (int iy = 0; iy <= 1; ++iy)
                    for (int ix = 0; ix <= 1; ++ix) {
                      grad_u[2][idim0] +=
                          x_batch[l](lfsu_2, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix) *
                          grad[ix + iy * 2 + iz * 4][0][idim0];
                      grad_u[1][idim0] +=
                          x_batch[l](lfsu_1, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix) *
                          grad[ix + iy * 2 + iz * 4][0][idim0];
                      grad_u[0][idim0] +=
                          x_batch[l](lfsu_0, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix) *
                          grad[ix + iy * 2 + iz * 4][0][idim0];
                    }
              }

              const auto div_u = grad_u[0][0] + grad_u[1][1] + grad_u[2][2];

              RF epsilon_u[3][3];
              for (int i = 0; i < 3; ++i) {
                for (int j = i; j < 3; ++j) {
                  epsilon_u[i][j] = 0.5 * (grad_u[i][j] + grad_u[j][i]);
                  epsilon_u[j][i] = epsilon_u[i][j];
                }
              }

              for (int iz = 0; iz <= 1; ++iz)
                for (int iy = 0; iy <= 1; ++iy)
                  for (int ix = 0; ix <= 1; ++ix) {
                    r_batch[l].accumulate(lfsu_2, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix,
                                          (1 * div_u * grad[ix + iy * 2 + iz * 4][0][2] +
                                           2.0 * (2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][0] * epsilon_u[0][2] +
                                                  2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][1] * epsilon_u[1][2] +
                                                  2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][2] * epsilon_u[2][2])) * factors[m]);
                    r_batch[l].accumulate(lfsu_1, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix,
                                          (1 * div_u * grad[ix + iy * 2 + iz * 4][0][1] +
                                           2.0 * (2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][0] * epsilon_u[0][1] +
                                                  2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][2] * epsilon_u[2][1] +
                                                  2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][1] * epsilon_u[1][1])) * factors[m]);
                    r_batch[l].accumulate(lfsu_0, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix,
                                          (1 * div_u * grad[ix + iy * 2 + iz * 4][0][0] +
                                           2.0 * (2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][1] * epsilon_u[1][0] +
                                                  2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][2] * epsilon_u[2][0] +
                                                  2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][0] * epsilon_u[0][0])) * factors[m]);
                  }
            }
      }
    };
    exec->launch( [=](pacxx::v2::range &config){ kernel(config); } , {{batch_size},{1}});
  }


  template<typename LFSV, typename EG, typename LFSU, typename R, typename X>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    alpha_volume(eg, lfsu, x, lfsv, r);
  }
};

#endif //SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_BATCHED_HH
