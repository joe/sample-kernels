// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_COMBINED_HH
#define SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_COMBINED_HH

#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/typetree/childextraction.hh"
#include "dune/localfunctions/lagrange/qk/qklocalbasis.hh"
#include "dune/pdelab/common/quadraturerules.hh"


// ADD PACXX
#include <PACXX.h>
#include <math.h>



template<typename GFSU, typename GFSV, typename RF, int k>
struct rOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern {

  enum { doPatternVolume = true };
  enum { doAlphaVolume = true };


  using GFSU_0 = Dune::TypeTree::Child<GFSU, 0>;
  using Q1_LocalBasis = Dune::QkLocalBasis<typename GFSU_0::Traits::GridView::ctype, double, 1, 3>;
  Dune::PDELab::LocalBasisCache<Q1_LocalBasis> cache_Q1;
  using JacobianType = typename Q1_LocalBasis::Traits::JacobianType;
  const Q1_LocalBasis Q1_microElementBasis;

  using coeffs_t = std::array<std::array<RF, 3>, (k+1)*(k+1)*(k+1)>;
  pacxx::v2::DeviceBuffer<coeffs_t> *buff_RXcontainer;
  mutable std::vector<coeffs_t> host_RXcontainer;
  struct returns_t {
    RF data[k][k][k][2][2][2][3] = {};
    struct index {
      std::array<int, 3> subel;
      std::array<int, 3> bnd;
      int comp;
    };
    RF &operator[](index i) {
      return data[i.subel[2]][i.subel[1]][i.subel[0]][i.bnd[2]][i.bnd[1]][i.bnd[0]][i.comp];
    };
  };
  pacxx::v2::DeviceBuffer<returns_t> *buff_returns;

  typename GFSU::Traits::GridView::template Codim<0>::Geometry::JacobianInverseTransposed jit;
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::ctype detjac;

  pacxx::v2::Executor* exec;

  using js_t = std::array<Dune::FieldMatrix<double,1,3>, 8>;
  pacxx::v2::DeviceBuffer<js_t> *buff_js_Q1;
  pacxx::v2::DeviceBuffer<double> *buff_factors;

  int qp_size;

  rOperator(const GFSU &gfsu, const GFSV &gfsv, size_t max_batch_size) :
      Q1_microElementBasis() {
    auto cell_geo = gfsu.gridView().template begin<0>()->geometry();
    const auto quadrature_rule = Dune::PDELab::quadratureRule(cell_geo, 2);

   // qp = quadrature_rule.begin(); // just assume a vector???
    qp_size = quadrature_rule.size();

    jit = cell_geo.jacobianInverseTransposed(Dune::FieldVector<RF, 3>());
    detjac = cell_geo.integrationElement(Dune::FieldVector<RF, 3>());

    for (const auto &qp: quadrature_rule) {
      cache_Q1.evaluateJacobian(qp.position(), Q1_microElementBasis);
    }

    exec = &pacxx::v2::Executor::get(0);

    buff_RXcontainer = &exec->allocate<coeffs_t>(max_batch_size);
    host_RXcontainer.resize(max_batch_size);
    buff_returns = &exec->allocate<returns_t>(max_batch_size);

    std::vector<js_t> js_Q1;
    js_Q1.reserve(qp_size);
    std::vector<double> factors;
    factors.reserve(qp_size);
    for (auto qp : quadrature_rule) {

      const auto & tmp = cache_Q1.evaluateJacobian(qp.position(), Q1_microElementBasis);
      js_Q1.emplace_back();
      std::copy(std::begin(tmp), std::end(tmp), std::begin(js_Q1.back()));

      factors.push_back(qp.weight() * detjac / double(k * k * k));
    }
    buff_js_Q1 = &exec->allocate<js_t>(qp_size);
    buff_js_Q1->upload(js_Q1);
    buff_factors = &exec->allocate<double>(qp_size);
    buff_factors->upload(factors);
  }


  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg_batch, const LFSU &lfsu_cache_batch, const X &x_batch,
                    const LFSV &lfsv_cache_batch, R &r_batch) const {

      auto cell_geo = eg_batch[0].entity().geometry();   // should depend on eg_batch and l

      const auto& lfsu = lfsu_cache_batch[0].localFunctionSpace(); // should depend on eg_batch and l
      const auto& lfsv = lfsv_cache_batch[0].localFunctionSpace(); // should depend on eg_batch and l
      using namespace Dune::Indices;
      auto lfsu_0 = Dune::TypeTree::child(lfsu, _0);
      auto lfsu_1 = Dune::TypeTree::child(lfsu, _1);
      auto lfsu_2 = Dune::TypeTree::child(lfsu, _2);

      size_t batch_size = eg_batch.size();

      for(auto l : Dune::range(batch_size))
        for(auto dof : Dune::range((k+1)*(k+1)*(k+1)))
        {
          host_RXcontainer[l][dof][0] = x_batch[l](lfsu_0, dof);
          host_RXcontainer[l][dof][1] = x_batch[l](lfsu_1, dof);
          host_RXcontainer[l][dof][2] = x_batch[l](lfsu_2, dof);
        }
      buff_RXcontainer->upload(host_RXcontainer);

      auto dRXContainer = buff_RXcontainer->get();
      auto dReturns = buff_returns->get();
      auto djs_Q1 = buff_js_Q1->get();
      auto dfactors = buff_factors->get();

      auto qp_size = this->qp_size;
      auto jit = this->jit;

      auto compute = [=](pacxx::v2::range &config)
    {

     int l = config.get_block(0);
     typename returns_t::index i;
     for(auto d : Dune::range(3))
       i.subel[d] = config.get_local(d);
     for(i.bnd[2] = 0; i.bnd[2] < 2; ++i.bnd[2])
       for(i.bnd[1] = 0; i.bnd[1] < 2; ++i.bnd[1])
         for(i.bnd[0] = 0; i.bnd[0] < 2; ++i.bnd[0])
           for(i.comp = 0; i.comp < 3; ++i.comp)
             dReturns[l][i] = 0.0;

     for (int m = 0; m < qp_size; ++m) 
     {
        JacobianType grad[8] = {};
        for (int basefunc = 0; basefunc < 8; ++basefunc)
          jit.usmv(k, djs_Q1[m][basefunc][0], grad[basefunc][0]);

        double grad_u[3][3] = {};

        for (int idim0 = 0; idim0 <= 2; ++idim0) {
          for (int iz = 0; iz <= 1; ++iz)
            for (int iy = 0; iy <= 1; ++iy)
              for (int ix = 0; ix <= 1; ++ix) {
                auto dofpos = (i.subel[1] + iy) * (k + 1) + (i.subel[2] + iz) * (k + 1) * (k + 1) + i.subel[0] + ix;
                grad_u[2][idim0] +=
                  dRXContainer[l][dofpos][2] *
                  grad[ix + iy * 2 + iz * 4][0][idim0];
                grad_u[1][idim0] +=
                  dRXContainer[l][dofpos][1] *
                  grad[ix + iy * 2 + iz * 4][0][idim0];
                grad_u[0][idim0] +=
                  dRXContainer[l][dofpos][0] *
                  grad[ix + iy * 2 + iz * 4][0][idim0];
              }
        }

        const auto div_u = grad_u[0][0] + grad_u[1][1] + grad_u[2][2];

        RF epsilon_u[3][3];
        for (int i = 0; i < 3; ++i) {
          for (int j = i; j < 3; ++j) {
            epsilon_u[i][j] = 0.5 * (grad_u[i][j] + grad_u[j][i]);
            epsilon_u[j][i] = epsilon_u[i][j];
          }
        }

        for (int iz = 0; iz <= 1; ++iz)
          for (int iy = 0; iy <= 1; ++iy)
            for (int ix = 0; ix <= 1; ++ix) {
              std::array<int, 3> bnd = { ix, iy, iz };
              dReturns[l][{i.subel,bnd,0}] +=
                (1 * div_u * grad[ix + iy * 2 + iz * 4][0][2] +
                 2.0 * (2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][0] * epsilon_u[0][2] +
                        2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][1] * epsilon_u[1][2] +
                        2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][2] * epsilon_u[2][2])) * dfactors[m];
              dReturns[l][{i.subel,bnd,1}] +=
                (1 * div_u * grad[ix + iy * 2 + iz * 4][0][1] +
                 2.0 * (2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][0] * epsilon_u[0][1] +
                        2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][2] * epsilon_u[2][1] +
                        2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][1] * epsilon_u[1][1])) * dfactors[m];
              dReturns[l][{i.subel,bnd,2}] +=
                (1 * div_u * grad[ix + iy * 2 + iz * 4][0][0] +
                 2.0 * (2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][1] * epsilon_u[1][0] +
                        2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][2] * epsilon_u[2][0] +
                        2 * 0.5 * grad[ix + iy * 2 + iz * 4][0][0] * epsilon_u[0][0])) * dfactors[m];
            }
      }
    };
   auto accumulate = [=](pacxx::v2::range &config)
    {

     int l = config.get_block(0);

     // - we iterate over the vertices with all {x,y,z} < k
     // - for each vertex, we iterate for each dimension over the lower and
     //   upper neighbor elements
     // - if any iteration would handle the lower neighbor element of a vertex
     //   with {x,y,z} == 0, it will handle the vertex with corresponding
     //   {x,y,z} == k instead

     std::array<int, 3> subvert;
     for(auto d : Dune::range(3)) subvert[d] = config.get_local(d);

     auto init_z = [&] (int vz) {
       auto init_y = [&] (int vy) {
         auto init_x = [&] (int vx) {
           auto dofpos = vy * (k + 1) + vz * (k + 1) * (k + 1) + vx;
           for(auto &v : dRXContainer[l][dofpos]) v = 0.0;
         };
         if(subvert[0] == 0) init_x(k);
         init_x(subvert[0]);
       };
       if(subvert[1] == 0) init_y(k);
       init_y(subvert[1]);
     };
     if(subvert[2] == 0) init_z(k);
     init_z(subvert[2]);

     std::array<int, 3> bnd;
     for (bnd[2] = 0; bnd[2] <= 1; ++bnd[2])
       for (bnd[1] = 0; bnd[1] <= 1; ++bnd[1])
         for (bnd[0] = 0; bnd[0] <= 1; ++bnd[0]) {
           std::array<int, 3> subel;
           for(auto d : Dune::range(3)) {
             subel[d] = subvert[d]+bnd[d]-1;
             if(subel[d] == -1) subel[d] += k;
           }
           auto dofpos = (subel[1]+bnd[1]) * (k + 1) + (subel[2]+bnd[2]) * (k + 1) * (k + 1) + (subel[0]+bnd[0]);
           dRXContainer[l][dofpos][2] += dReturns[l][{subel, bnd, 0}];
           dRXContainer[l][dofpos][1] += dReturns[l][{subel, bnd, 1}];
           dRXContainer[l][dofpos][0] += dReturns[l][{subel, bnd, 2}];
         }
    };
    exec->launch( compute,    {{batch_size},{k,k,k}});
    exec->launch( accumulate, {{batch_size},{k,k,k}});
    buff_RXcontainer->download(host_RXcontainer);
    for(auto l : Dune::range(batch_size))
      for(auto dof : Dune::range((k+1)*(k+1)*(k+1)))
      {
        r_batch[l].accumulate(lfsu_0, dof, host_RXcontainer[l][dof][0]);
        r_batch[l].accumulate(lfsu_1, dof, host_RXcontainer[l][dof][1]);
        r_batch[l].accumulate(lfsu_2, dof, host_RXcontainer[l][dof][2]);
      }
  }


  template<typename LFSV, typename EG, typename LFSU, typename R, typename X>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    alpha_volume(eg, lfsu, x, lfsv, r);
  }
};

#endif //SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_COMBINED_HH
