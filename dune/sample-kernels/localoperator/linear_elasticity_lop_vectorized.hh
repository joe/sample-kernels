// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_HH
#define SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_HH

#include <dune/vectorclass/vectorclass.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/typetree/childextraction.hh"
#include "dune/localfunctions/lagrange/qk/qklocalbasis.hh"
#include "dune/pdelab/common/quadraturerules.hh"

template<typename GFSU, typename GFSV, typename RF, int k>
struct rOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern {

  enum { doPatternVolume = true };
  enum { doAlphaVolume = true };


  using GFSU_0 = Dune::TypeTree::Child<GFSU, 0>;
  using Q1_LocalBasis = Dune::QkLocalBasis<typename GFSU_0::Traits::GridView::ctype, double, 1, 3>;
  Dune::PDELab::LocalBasisCache<Q1_LocalBasis> cache_Q1;
  using JacobianType = typename Q1_LocalBasis::Traits::JacobianType;
  const Q1_LocalBasis Q1_microElementBasis;

  typename GFSU::Traits::GridView::template Codim<0>::Geometry::JacobianInverseTransposed jit;
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::ctype detjac;


  rOperator(const GFSU &gfsu, const GFSV &gfsv) :
      Q1_microElementBasis() {
    jit = gfsu.gridView().template begin<0>()->geometry().jacobianInverseTransposed(Dune::FieldVector<RF, 3>());
    detjac = gfsu.gridView().template begin<0>()->geometry().integrationElement(Dune::FieldVector<RF, 3>());
  }


  template<typename LFSV, typename EG, typename LFSU, typename R, typename X>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    using namespace Dune::Indices;
    auto lfsu_0 = Dune::TypeTree::child(lfsu, _0);
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = Dune::PDELab::quadratureRule(cell_geo, 2);
    auto lfsu_1 = Dune::TypeTree::child(lfsu, _1);
    auto lfsu_2 = Dune::TypeTree::child(lfsu, _2);

    Vec4d x0, x1, x2;
    Vec4d r0, r1, r2;
    Vec4d right_node_0, right_node_2, right_node_1;

    for (const auto &qp: quadrature_rule) {
      const auto &phi_Q1 = cache_Q1.evaluateFunction(qp.position(), Q1_microElementBasis);
      const auto &js_Q1 = cache_Q1.evaluateJacobian(qp.position(), Q1_microElementBasis);

      const auto factor = r.weight() * qp.weight() * detjac / double(k * k * k);
      Vec4d factor_vec(factor);

      std::array<JacobianType, 8> grad = {};
      for (int i = 0; i < 8; ++i)
        jit.usmv(k, js_Q1[i][0], grad[i][0]);

      for (int subel_z = 0; subel_z < k; ++subel_z)
        for (int subel_y = 0; subel_y < k; ++subel_y) {
          Vec4d left_node_0[4] = {0, 0, 0, 0};
          Vec4d left_node_1[4] = {0, 0, 0, 0};
          Vec4d left_node_2[4] = {0, 0, 0, 0};
          for (int subel_x = 0; subel_x < k; subel_x += 4) {

            Vec4d grad_u[3][3];

            for (int idim0 = 0; idim0 <= 2; ++idim0) {
              grad_u[2][idim0] = 0.0;
              grad_u[1][idim0] = 0.0;
              grad_u[0][idim0] = 0.0;
              for (int iz = 0; iz <= 1; ++iz)
                for (int iy = 0; iy <= 1; ++iy)
                  for (int ix = 0; ix <= 1; ++ix) {
                    x2.load(&x(lfsu_2, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix));
                    x1.load(&x(lfsu_1, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix));
                    x0.load(&x(lfsu_0, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x + ix));
                    grad_u[2][idim0] += x2 * grad[ix + iy * 2 + iz * 4][0][idim0];
                    grad_u[1][idim0] += x1 * grad[ix + iy * 2 + iz * 4][0][idim0];
                    grad_u[0][idim0] += x0 * grad[ix + iy * 2 + iz * 4][0][idim0];
                  }
            }

            Vec4d div_u = grad_u[0][0] + grad_u[1][1] + grad_u[2][2];

            Vec4d epsilon_u[3][3];
            for (int i = 0; i < 3; ++i) {
              for (int j = i; j < 3; ++j) {
                epsilon_u[i][j] = 0.5 * (grad_u[i][j] + grad_u[j][i]);
                epsilon_u[j][i] = epsilon_u[i][j];
              }
            }

            for (int iz = 0; iz <= 1; ++iz)
              for (int iy = 0; iy <= 1; ++iy) {
                r2.load(&r.container()(lfsu_2, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x));
                right_node_2 = 1 * div_u * grad[1 + iy * 2 + iz * 4][0][2] +
                               2.0 * (grad[1 + iy * 2 + iz * 4][0][0] * epsilon_u[0][2] +
                                      grad[1 + iy * 2 + iz * 4][0][1] * epsilon_u[1][2] +
                                      grad[1 + iy * 2 + iz * 4][0][2] * epsilon_u[2][2]);
                left_node_2[2 * iz + iy] += 1 * div_u * grad[iy * 2 + iz * 4][0][2] +
                                   2.0 * (grad[iy * 2 + iz * 4][0][0] * epsilon_u[0][2] +
                                          grad[iy * 2 + iz * 4][0][1] * epsilon_u[1][2] +
                                          grad[iy * 2 + iz * 4][0][2] * epsilon_u[2][2]);
                left_node_2[2 * iz + iy] += permute4d<-1,0,1,2>(right_node_2);

                r2 += left_node_2[2 * iz + iy] * factor_vec;
                r2.store(&r.container()(lfsu_2, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x));
                left_node_2[2* iz + iy] = permute4d<3,-1,-1,-1>(right_node_2);

                ///////////////////////////////////////////////////////////////////////////////////////////////////////

                r1.load(&r.container()(lfsu_1, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x));
                right_node_1 = 1 * div_u * grad[1 + iy * 2 + iz * 4][0][1] +
                               2.0 * (grad[1 + iy * 2 + iz * 4][0][0] * epsilon_u[0][1] +
                                      grad[1 + iy * 2 + iz * 4][0][2] * epsilon_u[2][1] +
                                      grad[1 + iy * 2 + iz * 4][0][1] * epsilon_u[1][1]);
                left_node_1[2 * iz + iy] += 1 * div_u * grad[iy * 2 + iz * 4][0][1] +
                                   2.0 * (grad[iy * 2 + iz * 4][0][0] * epsilon_u[0][1] +
                                          grad[iy * 2 + iz * 4][0][2] * epsilon_u[2][1] +
                                          grad[iy * 2 + iz * 4][0][1] * epsilon_u[1][1]);
                left_node_1[2 * iz + iy] += permute4d<-1,0,1,2>(right_node_1);
                r1 += left_node_1[2 * iz + iy] * factor_vec;
                r1.store(&r.container()(lfsu_1, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x));
                left_node_1[2* iz + iy] = permute4d<3,-1,-1,-1>(right_node_1);

                ///////////////////////////////////////////////////////////////////////////////////////////////////////

                r0.load(&r.container()(lfsu_0, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x));
                right_node_0 = div_u * grad[1 + iy * 2 + iz * 4][0][0] +
                           2.0 * (grad[1 + iy * 2 + iz * 4][0][1] * epsilon_u[1][0] +
                                  grad[1 + iy * 2 + iz * 4][0][2] * epsilon_u[2][0] +
                                  grad[1 + iy * 2 + iz * 4][0][0] * epsilon_u[0][0]);
                left_node_0[2 * iz + iy] += 1 * div_u * grad[iy * 2 + iz * 4][0][0] +
                                            2.0 * (grad[iy * 2 + iz * 4][0][1] * epsilon_u[1][0] +
                                                   grad[iy * 2 + iz * 4][0][2] * epsilon_u[2][0] +
                                                   grad[iy * 2 + iz * 4][0][0] * epsilon_u[0][0]);
                left_node_0[2 * iz + iy] += permute4d<-1,0,1,2>(right_node_0);
                r0 += left_node_0[2 * iz + iy] * factor_vec;
                r0.store(&r.container()(lfsu_0, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + subel_x));
                left_node_0[2* iz + iy] = permute4d<3,-1,-1,-1>(right_node_0);
              }
          }
          for (int iz = 0; iz <= 1; ++iz)
            for (int iy = 0; iy <= 1; ++iy) {
              r.accumulate(lfsu_2, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + k,
                           left_node_2[2* iz + iy][0] * factor);
              r.accumulate(lfsu_1, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + k,
                           left_node_1[2* iz + iy][0] * factor);
              r.accumulate(lfsu_0, (subel_y + iy) * (k + 1) + (subel_z + iz) * (k + 1) * (k + 1) + k,
                           left_node_0[2* iz + iy][0] * factor);
            }
        }
    }
  }


  template<typename LFSV, typename EG, typename LFSU, typename R, typename X>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    alpha_volume(eg, lfsu, x, lfsv, r);
  }
};

#endif //SAMPLE_KERNELS_LINEAR_ELASTICITY_LOP_HH
