// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef SAMPLE_KERNELS_POISSON_LOP_HH
#define SAMPLE_KERNELS_POISSON_LOP_HH


#include "dune/localfunctions/lagrange/qk/qklocalbasis.hh"
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/pdelab/finiteelement/localbasiscache.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/pdelab/localoperator/defaultimp.hh"

template<typename GFSU, typename GFSV, typename RF, int k>
struct rOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::NumericalJacobianVolume<rOperator<GFSU,GFSV,RF,k>>
{
  enum { doPatternVolume = true };
  enum { doAlphaVolume = true };

  using Q1_LocalBasis = Dune::QkLocalBasis<typename GFSV::Traits::GridView::ctype, double, 1, 2>;
  using JacobianType = typename Q1_LocalBasis::Traits::JacobianType;
  Dune::PDELab::LocalBasisCache<Q1_LocalBasis> cache_Q1;
  const Q1_LocalBasis Q1_microElementBasis;

  typename GFSU::Traits::GridView::template Codim<0>::Geometry::JacobianInverseTransposed jit;
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::ctype detjac;


  rOperator(const GFSU &gfsu, const GFSV &gfsv) :
      Q1_microElementBasis() {
    jit = gfsu.gridView().template begin<0>()->geometry().jacobianInverseTransposed(Dune::FieldVector<RF, 2>());
    detjac = gfsu.gridView().template begin<0>()->geometry().integrationElement(Dune::FieldVector<RF, 2>());
  }


  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = Dune::PDELab::quadratureRule(cell_geo, 2);

    for (const auto &qp: quadrature_rule) {
      const auto &js_Q1 = cache_Q1.evaluateJacobian(qp.position(), Q1_microElementBasis);

      // compute gradients of basis functions in transformed element
      std::array<JacobianType, 4> grad = {};
      for (int i = 0; i < 4; ++i) {
        jit.usmv(k, js_Q1[i][0], grad[i][0]);
      }

      const RF factor = detjac * qp.weight() / double(k * k);

      for (int subel_y = 0; subel_y < k; ++subel_y)
        for (int subel_x = 0; subel_x < k; ++subel_x) {

          RF gradu[2] = {};
          for (int idim0 = 0; idim0 <= 1; ++idim0) {
            for (int iy = 0; iy < 2; ++iy)
              for (int ix = 0; ix < 2; ++ix)
                gradu[idim0] += x(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix) * grad[ix + iy * 2][0][idim0];
          }

          for (int iy = 0; iy < 2; ++iy)
            for (int ix = 0; ix < 2; ++ix)
              r.accumulate(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix,
                           (grad[ix + iy * 2][0][0] * gradu[0] +
                            grad[ix + iy * 2][0][1] * gradu[1]) * factor);
        }
    }
  }


  template<typename EG, typename LFSU, typename X, typename LFSV, typename Y>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, Y &y) const
  {
    alpha_volume(eg, lfsu, x, lfsv, y);
  }
};

#endif //SAMPLE_KERNELS_POISSON_LOP_HH
