// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_STOKES_LOP_HH
#define DUNE_STOKES_LOP_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/typetree/childextraction.hh"
#include "dune/localfunctions/lagrange/qk/qklocalbasis.hh"
#include "dune/pdelab/finiteelement/localbasiscache.hh"
#include "dune/pdelab/common/quadraturerules.hh"


template<typename GFSU, typename GFSV, typename RF, int k>
struct LocalOperatorR
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::NumericalJacobianVolume<LocalOperatorR<GFSU, GFSV, RF, k>>
{
  enum {
    doPatternVolume = true
  };
  enum {
    doAlphaVolume = true
  };

  using GFSU_1 = Dune::TypeTree::Child<GFSU, 1>;
  using Q1_LocalBasis = Dune::QkLocalBasis<typename GFSU_1::Traits::GridView::ctype, RF, 1, 2>;

  using GFSU_0 = Dune::TypeTree::Child<GFSU, 0>;
  using GFSU_0_0 = Dune::TypeTree::Child<GFSU_0, 0>;
  using Q2_LocalBasis = Dune::QkLocalBasis<typename GFSU_0_0::Traits::GridView::ctype, RF, 2, 2>;
  using JacobianType = typename Q2_LocalBasis::Traits::JacobianType;

  Dune::PDELab::LocalBasisCache<Q1_LocalBasis> cache_Q1;
  const Q1_LocalBasis Q1_microElementBasis;

  Dune::PDELab::LocalBasisCache<Q2_LocalBasis> cache_Q2;
  const Q2_LocalBasis Q2_microElementBasis;

  typename GFSU::Traits::GridView::template Codim<0>::Geometry::JacobianInverseTransposed jit;
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::ctype detjac;


  LocalOperatorR(const GFSU &gfsu, const GFSV &gfsv) :
      Q1_microElementBasis(),
      Q2_microElementBasis()
  {
    jit = gfsu.gridView().template begin<0>()->geometry().jacobianInverseTransposed(Dune::FieldVector<RF, 2>());
    detjac = gfsu.gridView().template begin<0>()->geometry().integrationElement(Dune::FieldVector<RF, 2>());
  }

public:
  template<typename R, typename LFSV, typename LFSU, typename X, typename EG>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const
  {
    using namespace Dune::Indices;
    auto lfsu_p = Dune::TypeTree::child(lfsu, _1);
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = Dune::PDELab::quadratureRule(cell_geo, 3);
    auto lfsu_u = Dune::TypeTree::child(lfsu, _0);
    auto lfsu_u_0 = Dune::TypeTree::child(lfsu_u, _0);
    auto lfsu_u_1 = Dune::TypeTree::child(lfsu_u, _1);

    for (const auto &qp: quadrature_rule) {
      const auto &js_Q2 = cache_Q2.evaluateJacobian(qp.position(), Q2_microElementBasis);
      const auto &phi_Q1 = cache_Q1.evaluateFunction(qp.position(), Q1_microElementBasis);

      const auto factor = qp.weight() * detjac / double(k * k);

      std::array<JacobianType, 9> grad = {};
      for (int i = 0; i < 9; ++i)
        jit.usmv(k, js_Q2[i][0], grad[i][0]);

      for (int subel_y = 0; subel_y < k; ++subel_y) {
        for (int subel_x = 0; subel_x < k; ++subel_x) {

          RF p = 0;
          for (int iy = 0; iy <= 1; ++iy)
            for (int ix = 0; ix <= 1; ++ix)
              p += x(lfsu_p, (subel_y + iy) * (k + 1) + subel_x + ix) * phi_Q1[ix + iy * 2][0];

          RF grad_u[2][2] = {};
          for (int idim0 = 0; idim0 <= 1; ++idim0) {
            for (int iy = 0; iy <= 2; ++iy)
              for (int ix = 0; ix <= 2; ++ix) {
                grad_u[1][idim0] +=
                    x(lfsu_u_1, (2 * subel_y + iy) * (2 * k + 1) + 2 * subel_x + ix) * grad[ix + iy * 3][0][idim0];
                grad_u[0][idim0] +=
                    x(lfsu_u_0, (2 * subel_y + iy) * (2 * k + 1) + 2 * subel_x + ix) * grad[ix + iy * 3][0][idim0];
              }
          }

          for (int iy = 0; iy <= 1; ++iy)
            for (int ix = 0; ix <= 1; ++ix)
              r.accumulate(lfsu_p, (subel_y + iy) * (k + 1) + subel_x + ix,
                           -1.0 * phi_Q1[ix + iy * 2][0] * (grad_u[0][0] + grad_u[1][1]) * factor);
          for (int iy = 0; iy <= 2; ++iy)
            for (int ix = 0; ix <= 2; ++ix) {
              r.accumulate(lfsu_u_1, (2 * subel_y + iy) * (2 * k + 1) + 2 * subel_x + ix,
                           (-1.0 * p * grad[ix + iy * 3][0][1] + grad[ix + iy * 3][0][0] * grad_u[1][0] +
                            grad[ix + iy * 3][0][1] * grad_u[1][1]) * factor);
              r.accumulate(lfsu_u_0, (2 * subel_y + iy) * (2 * k + 1) + 2 * subel_x + ix,
                           (-1.0 * p * grad[ix + iy * 3][0][0] + grad[ix + iy * 3][0][0] * grad_u[0][0] +
                            grad[ix + iy * 3][0][1] * grad_u[0][1]) * factor);
            }
        }
      }
    }
  }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Y>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, Y &y) const
  {
    alpha_volume(eg, lfsu, x, lfsv, y);
  }
};

#endif
