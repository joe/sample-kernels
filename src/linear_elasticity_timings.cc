#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <chrono>
#include <sstream>
#include <sys/types.h>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include "dune/common/parallel/mpihelper.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/common/parametertree.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/grid/io/file/vtk.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/gridoperator/blockstructured.hh"
#include "dune/pdelab/common/partitionviewentityset.hh"
#include "dune/pdelab/common/functionutilities.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/pdelab/gridfunctionspace/subspace.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/sample-kernels/common/matrixfree.hh"
#include "dune/sample-kernels/common/blockstructuredqkfem.hh"


#ifdef VECTORIZE
#include "dune/sample-kernels/localoperator/linear_elasticity_lop_vectorized.hh"
#else
#ifdef BATCHED
#include "dune/sample-kernels/gridoperator/batchedgridoperator.hh"
#ifdef COMBINED
#include "dune/sample-kernels/localoperator/linear_elasticity_lop_combined.hh"
#else
#include "dune/sample-kernels/localoperator/linear_elasticity_lop_batched.hh"
#endif
#else
#include "dune/sample-kernels/localoperator/linear_elasticity_lop.hh"
#endif
#endif


#include "nblocks.hh"



int main(int argc, char** argv){
  try
  {
    // Initialize basic stuff...
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    using RangeType = double;

    // Setup grid (view)...
    using Grid = Dune::YaspGrid<3, Dune::EquidistantCoordinates<RangeType, 3>>;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    unsigned int cellsPerDir = 1;
    if (argc > 1) {
      cellsPerDir = strtoul(argv[1],NULL,0);
    }
    std::shared_ptr<Grid> grid(Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, 3>{0,0,0},
                                                                                 Dune::FieldVector<double, 3>{1,0.2,0.2},
                                                                                 std::array<unsigned int, 3>{5*cellsPerDir,cellsPerDir,cellsPerDir}));
    GV gv = grid->leafGridView();

    // Set up finite element maps...
    using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, NBLOCKS>;
    FEM fem(gv);

    // Set up grid function spaces...
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
    using EmptyConstraints = Dune::PDELab::EmptyTransformation;
    using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, EmptyConstraints, VectorBackend>;
    using POW3GFS = Dune::PDELab::PowerGridFunctionSpace<GFS, 3, VectorBackend, Dune::PDELab::LexicographicOrderingTag>;
    GFS gfs(gv, fem);
    gfs.name("gfs");
    POW3GFS pow3gfs(gfs);
    using namespace Dune::Indices;
    pow3gfs.child(_0).name("pow3gfs_0");
    pow3gfs.child(_1).name("pow3gfs_1");
    pow3gfs.child(_2).name("pow3gfs_2");


    // Set up grid grid operators...
    using LOP = rOperator<POW3GFS, POW3GFS, RangeType, NBLOCKS>;
    using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
#ifdef BATCHED
    using GO = Dune::PDELab::BatchedGridOperator<POW3GFS, POW3GFS, LOP , MatrixBackend, DF, RangeType, RangeType>;
    int batch_size = 1;
    if (argc > 2) {
      batch_size = strtoul(argv[2],NULL,0);
    }
#ifdef COMBINED
    LOP lop(pow3gfs, pow3gfs, batch_size);
#else
    LOP lop(pow3gfs, pow3gfs);
#endif
#else
    using GO = Dune::PDELab::BlockstructuredGridOperator<POW3GFS, POW3GFS, LOP, MatrixBackend, DF, RangeType, RangeType>;
    LOP lop(pow3gfs, pow3gfs);
#endif
    pow3gfs.update();
#ifdef BATCHED
    GO go(batch_size, pow3gfs, pow3gfs, lop, MatrixBackend(0));
#else
    GO go(pow3gfs, pow3gfs, lop, MatrixBackend(0));
#endif
    std::cout << "gfs with " << pow3gfs.size() << " dofs generated  "<< std::endl;


    // Set up solution vectors...
    using V = Dune::PDELab::Backend::Vector<POW3GFS,DF>;
    V x(pow3gfs, 1.), r(pow3gfs, 0.);

    auto start = std::chrono::high_resolution_clock::now();
    go.jacobian_apply(x, r);
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> dt = end - start;
    std::cout << "Time for global operator application: " << dt.count() << std::endl;

  }
  catch (Dune::Exception& e)
  {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << "Unknown exception thrown: " << e.what() << std::endl;
    return 1;
  }
}
