#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <chrono>
#include <sstream>
#include <sys/types.h>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include "dune/common/parallel/mpihelper.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/common/parametertree.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/grid/io/file/vtk.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/common/partitionviewentityset.hh"
#include "dune/pdelab/common/functionutilities.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/pdelab/gridfunctionspace/subspace.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/sample-kernels/common/matrixfree.hh"
#include "dune/sample-kernels/common/blockstructuredqkfem.hh"


#ifdef VECTORIZE
#include "dune/sample-kernels/localoperator/linear_elasticity_lop_vectorized.hh"
#else
#ifdef BATCHED
#include "dune/sample-kernels/gridoperator/batchedgridoperator.hh"
#ifdef COMBINED
#include "dune/sample-kernels/localoperator/linear_elasticity_lop_combined.hh"
#else
#include "dune/sample-kernels/localoperator/linear_elasticity_lop_batched.hh"
#endif
#else
#include "dune/sample-kernels/localoperator/linear_elasticity_lop.hh"
#endif
#endif

#include "nblocks.hh"



int main(int argc, char** argv){
  try
  {
    // Initialize basic stuff...
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    using RangeType = double;

    // Setup grid (view)...
    using Grid = Dune::YaspGrid<3, Dune::EquidistantCoordinates<RangeType, 3>>;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    unsigned int cellsPerDir = 1;
    if (argc > 1) {
      cellsPerDir = strtoul(argv[1],NULL,0);
    }
    std::shared_ptr<Grid> grid(Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, 3>{0,0,0},
                                                                                 Dune::FieldVector<double, 3>{1,0.2,0.2},
                                                                                 std::array<unsigned int, 3>{5*cellsPerDir,cellsPerDir,cellsPerDir}));
    GV gv = grid->leafGridView();

    // Set up finite element maps...
    using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, NBLOCKS>;
    FEM fem(gv);

    // Set up grid function spaces...
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
    using DirichletConstraintsAssember = Dune::PDELab::ConformingDirichletConstraints;
    using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, DirichletConstraintsAssember, VectorBackend>;
    using POW3GFS = Dune::PDELab::PowerGridFunctionSpace<GFS, 3, VectorBackend, Dune::PDELab::LexicographicOrderingTag>;
    GFS gfs(gv, fem);
    gfs.name("gfs");
    POW3GFS pow3gfs(gfs);
    using namespace Dune::Indices;
    pow3gfs.child(_0).name("pow3gfs_0");
    pow3gfs.child(_1).name("pow3gfs_1");
    pow3gfs.child(_2).name("pow3gfs_2");

    // Set up constraints container...
    using CC = POW3GFS::ConstraintsContainer<RangeType>::Type;
    CC cc;
    cc.clear();
//    auto bctype_lambda = [&](const auto& x){ return (x[0] < 1e-08 ? 1 : 0.0); };
    auto bctype_lambda = [&](const auto& x){ return 1; };
    auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, bctype_lambda);
    Dune::PDELab::PowerConstraintsParameters<decltype(bctype), 3> pow3bctype(bctype);
    Dune::PDELab::constraints(pow3bctype, pow3gfs, cc);

    // Set up grid grid operators...
    using LOP = rOperator<POW3GFS, POW3GFS, RangeType, NBLOCKS>;
    using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
#ifdef BATCHED
    using GO = Dune::PDELab::BatchedGridOperator<POW3GFS, POW3GFS, LOP , MatrixBackend, DF, RangeType, RangeType, CC, CC>;
    int batch_size = 1;
    if (argc > 2) {
      batch_size = strtoul(argv[2],NULL,0);
    }
#ifdef COMBINED
    LOP lop(pow3gfs, pow3gfs, batch_size);
#else
    LOP lop(pow3gfs, pow3gfs);
#endif
#else
    using GO = Dune::PDELab::GridOperator<POW3GFS, POW3GFS, LOP, MatrixBackend, DF, RangeType, RangeType, CC, CC>;
    LOP lop(pow3gfs, pow3gfs);
#endif
    pow3gfs.update();
    std::size_t dofestimate =  8 * pow3gfs.maxLocalSize();
    MatrixBackend mb(dofestimate);

#ifdef BATCHED
    GO go(batch_size, pow3gfs, cc, pow3gfs, cc, lop, mb);
#else
    GO go(pow3gfs, cc, pow3gfs, cc, lop, mb);
#endif
    std::cout << "gfs with " << pow3gfs.size() << " dofs generated  "<< std::endl;
    std::cout << "cc with " << cc.size() << " dofs generated  "<< std::endl;

    // Set up solution vectors...
    using V = Dune::PDELab::Backend::Vector<POW3GFS,DF>;
    V x(pow3gfs);
    x = 0.0;
    auto lambda_x = [&](const auto& is, const auto& xl){ const auto x=is.geometry().global(xl); return -x[0]; };
    auto lambda_0 = [&](const auto& x){ return 0; };
    auto func_x = Dune::PDELab::makeGridFunctionFromCallable(gv, lambda_x);
    auto func_0 = Dune::PDELab::makeGridFunctionFromCallable(gv, lambda_0);
    Dune::PDELab::CompositeGridFunction<decltype(func_0), decltype(func_0), decltype(func_x)> solution(func_0, func_0, func_x);
    Dune::PDELab::interpolate(solution, pow3gfs, x);

    // Set up (non)linear solvers...
    Dune::PDELab::solveMatrixFreeBiCGSTAB(go, x);

    // Do visualization...
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    Dune::RefinementIntervals subint(NBLOCKS);
    VTKWriter vtkwriter(gv, subint);
    std::string vtkfile = "linear_elasticity";
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter, pow3gfs, x, Dune::PDELab::vtk::defaultNameScheme());
    vtkwriter.write(vtkfile, Dune::VTK::ascii);

    Dune::PDELab::GridFunctionSubSpace<POW3GFS, Dune::TypeTree::TreePath<0> > subSpace_0(pow3gfs);
    Dune::PDELab::GridFunctionSubSpace<POW3GFS, Dune::TypeTree::TreePath<1> > subSpace_1(pow3gfs);
    Dune::PDELab::GridFunctionSubSpace<POW3GFS, Dune::TypeTree::TreePath<2> > subSpace_2(pow3gfs);


    // Maybe calculate errors for test results...
    using DGF_0 = Dune::PDELab::DiscreteGridFunction<decltype(subSpace_0),decltype(x)>;
    DGF_0 dgf_0(subSpace_0,x);
    using DifferenceSquaredAdapter_0_0 = Dune::PDELab::DifferenceSquaredAdapter<decltype(func_0), decltype(dgf_0)>;
    DifferenceSquaredAdapter_0_0 dsa_0_0(func_0, dgf_0);
    Dune::FieldVector<RangeType, 1> l2error(0.0);
    {
      // L2 error squared of difference between numerical
      // solution and the interpolation of exact solution
      // for treepath (0, 0)
      typename decltype(dsa_0_0)::Traits::RangeType err(0.0);
      Dune::PDELab::integrateGridFunction(dsa_0_0, err, 10);

      l2error += err;
      if (gv.comm().rank() == 0){
        std::cout << "L2 Error for treepath 0: " << err << std::endl;
      }
    }
    using DGF_1 = Dune::PDELab::DiscreteGridFunction<decltype(subSpace_1),decltype(x)>;
    DGF_1 dgf_1(subSpace_1,x);
    using DifferenceSquaredAdapter_0_1 = Dune::PDELab::DifferenceSquaredAdapter<decltype(func_0), decltype(dgf_1)>;
    DifferenceSquaredAdapter_0_1 dsa_0_1(func_0, dgf_1);
    {
      // L2 error squared of difference between numerical
      // solution and the interpolation of exact solution
      // for treepath (0, 1)
      typename decltype(dsa_0_1)::Traits::RangeType err(0.0);
      Dune::PDELab::integrateGridFunction(dsa_0_1, err, 10);

      l2error += err;
      if (gv.comm().rank() == 0){
        std::cout << "L2 Error for treepath 1: " << err << std::endl;
      }
    }
    using DGF_2 = Dune::PDELab::DiscreteGridFunction<decltype(subSpace_2),decltype(x)>;
    DGF_2 dgf_2(subSpace_2,x);
    using DifferenceSquaredAdapter_1 = Dune::PDELab::DifferenceSquaredAdapter<decltype(func_x), decltype(dgf_2)>;
    DifferenceSquaredAdapter_1 dsa_1(func_x, dgf_2);
    {
      // L2 error squared of difference between numerical
      // solution and the interpolation of exact solution
      // for treepath (1)
      typename decltype(dsa_1)::Traits::RangeType err(0.0);
      Dune::PDELab::integrateGridFunction(dsa_1, err, 10);

      l2error += err;
      if (gv.comm().rank() == 0){
        std::cout << "L2 Error for treepath 2: " << err << std::endl;
      }
    }
    if (gv.comm().rank() == 0){
      std::cout << "\nl2errorsquared: " << l2error << std::endl << std::endl;
    }

  }
  catch (Dune::Exception& e)
  {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << "Unknown exception thrown: " << e.what() << std::endl;
    return 1;
  }
}
