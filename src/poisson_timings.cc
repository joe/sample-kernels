// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>
#include <dune/sample-kernels/common/matrixfree.hh>

#include "dune/common/parallel/mpihelper.hh"
#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/sample-kernels/common/blockstructuredqkfem.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/gridoperator/blockstructured.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/pdelab/common/functionutilities.hh"
#include "dune/pdelab/constraints/common/constraintstransformation.hh"
#include "dune/pdelab/gridfunctionspace/localfunctionspace.hh"
#include "dune/pdelab/gridfunctionspace/lfsindexcache.hh"

#ifdef VECTORIZE
#include "dune/sample-kernels/localoperator/poisson_lop_vectorized.hh"
#else
#ifdef BATCHED
#include "dune/sample-kernels/gridoperator/batchedgridoperator.hh"
#include "dune/sample-kernels/localoperator/poisson_lop_batched.hh"
#else
#include "dune/sample-kernels/localoperator/poisson_lop.hh"
#endif
#endif

#define NBLOCKS 32

int main(int argc, char** argv){
  try
  {
    // Initialize basic stuff...
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    using RangeType = double;

    // Setup grid (view)...
    using Grid = Dune::YaspGrid<2, Dune::EquidistantCoordinates<RangeType, 2>>;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    unsigned int cellsPerDir = 1;
    if (argc > 1) {
      cellsPerDir = strtoul(argv[1],NULL,0);
    }
    std::shared_ptr<Grid> grid(Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, 2>{0,0},
                                                                                 Dune::FieldVector<double, 2>{1,1},
                                                                                 std::array<unsigned int, 2>{cellsPerDir,cellsPerDir}));
    GV gv = grid->leafGridView();

    // Set up finite element maps...
    using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, NBLOCKS>;
    FEM fem(gv);

    // Set up grid function spaces...
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
    using EmptyConstraints = Dune::PDELab::EmptyTransformation;
    using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, EmptyConstraints, VectorBackend>;
    GFS gfs(gv, fem);
    gfs.name("gfs");

    // Set up grid grid operators...
    using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
    using LOP = rOperator<GFS, GFS, RangeType, NBLOCKS>;
#ifdef BATCHED
    using GO = Dune::PDELab::BatchedGridOperator<GFS, GFS, LOP , MatrixBackend, DF, RangeType, RangeType>;
#else
    using GO = Dune::PDELab::BlockstructuredGridOperator<GFS, GFS, LOP, MatrixBackend, DF, RangeType, RangeType>;
#endif
    LOP lop(gfs, gfs);
    gfs.update();
#ifdef BATCHED
    int batch_size = 1;
    if (argc > 2) {
      batch_size = strtoul(argv[2],NULL,0);
    }
    GO go(batch_size, gfs, gfs, lop, MatrixBackend(0));
#else
    GO go(gfs, gfs, lop, MatrixBackend(0));
#endif
    std::cout << "gfs with " << gfs.size() << " dofs generated  "<< std::endl;

    // Set up solution vectors...
    using V = Dune::PDELab::Backend::Vector<GFS,DF>;
    V x(gfs, 1.), r(gfs, 0.);

    auto start = std::chrono::high_resolution_clock::now();
    go.jacobian_apply(x, r);
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> dt = end - start;
    std::cout << "Time for global operator application: " << dt.count() << std::endl;

  }
  catch (Dune::Exception& e)
  {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << "Unknown exception thrown: " << e.what() << std::endl;
    return 1;
  }
}
