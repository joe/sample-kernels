// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>
#include <dune/sample-kernels/common/matrixfree.hh>

#include "dune/common/parallel/mpihelper.hh"
#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/sample-kernels/common/blockstructuredqkfem.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/pdelab/common/functionutilities.hh"

#ifdef VECTORIZE
#include "dune/sample-kernels/localoperator/poisson_lop_vectorized.hh"
#else
#ifdef BATCHED
#include "dune/sample-kernels/gridoperator/batchedgridoperator.hh"
#include "dune/sample-kernels/localoperator/poisson_lop_batched.hh"
#else
#include "dune/sample-kernels/localoperator/poisson_lop.hh"
#endif
#endif

#include "nblocks.hh"


int main(int argc, char** argv){
  try
  {
    // Initialize basic stuff...
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    using RangeType = double;

    // Setup grid (view)...
    using Grid = Dune::YaspGrid<2, Dune::EquidistantCoordinates<RangeType, 2>>;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    unsigned int cellsPerDir = 1;
    if (argc > 1) {
      cellsPerDir = strtoul(argv[1],NULL,0);
    }
    std::shared_ptr<Grid> grid(Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, 2>{0,0},
                                                                                 Dune::FieldVector<double, 2>{1,1},
                                                                                 std::array<unsigned int, 2>{cellsPerDir,cellsPerDir}));
    GV gv = grid->leafGridView();


    // Set up finite element maps...
    using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, NBLOCKS>;
    FEM fem(gv);

    // Set up grid function spaces...
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
    using DirichletConstraintsAssember = Dune::PDELab::ConformingDirichletConstraints;
    using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, DirichletConstraintsAssember, VectorBackend>;
    GFS gfs(gv, fem);
    gfs.name("gfs");

    // Set up constraints container...
    using CC = GFS::ConstraintsContainer<RangeType>::Type;
    CC cc;
    cc.clear();
    auto bctype_lambda = [&](const auto& x){ return 1.0; };
    auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, bctype_lambda);
    Dune::PDELab::constraints(bctype, gfs, cc);

    // Set up grid grid operators...
    using LOP = rOperator<GFS, GFS, RangeType, NBLOCKS>;
    using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
#ifdef BATCHED
    using GO = Dune::PDELab::BatchedGridOperator<GFS, GFS, LOP , MatrixBackend, DF, RangeType, RangeType, CC, CC>;
#else
    using GO = Dune::PDELab::GridOperator<GFS, GFS, LOP, MatrixBackend, DF, RangeType, RangeType, CC, CC>;
#endif
    LOP lop(gfs, gfs);
    gfs.update();
    std::size_t dofestimate =  4 * gfs.maxLocalSize();
    MatrixBackend mb(dofestimate);

#ifdef BATCHED
    int batch_size = 1;
    if (argc > 2) {
      batch_size = strtoul(argv[2],NULL,0);
    }
    GO go(batch_size, gfs, cc, gfs, cc, lop, mb);
#else
    GO go(gfs, cc, gfs, cc, lop, mb);
#endif
    std::cout << "gfs with " << gfs.size() << " dofs generated  "<< std::endl;
    std::cout << "cc with " << cc.size() << " dofs generated  "<< std::endl;

    // Set up solution vectors...
    using V = Dune::PDELab::Backend::Vector<GFS,DF>;
    V x(gfs);
    x = 0.0;
    auto lambda_0000 = [&](const auto& x){ return (double)x[1] * x[1] - x[0] * x[0]; };
    auto func_0000 = Dune::PDELab::makeGridFunctionFromCallable(gv, lambda_0000);
    Dune::PDELab::interpolate(func_0000, gfs, x);

    // Set up (non)linear solvers...
//    using LinearSolver = Dune::PDELab::ISTLBackend_BCGS_AMG_SSOR<GO>;
//    using SLP = Dune::PDELab::StationaryLinearProblemSolver<GO, LinearSolver, V>;
//    LinearSolver ls(gfs);
//    double reduction = 1e-8;
//    SLP slp(go, ls, x, reduction);
//    slp.apply();
    Dune::PDELab::solveMatrixFreeBiCGSTAB(go, x);

    // Do visualization...
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    Dune::RefinementIntervals subint(NBLOCKS);
    VTKWriter vtkwriter(gv, subint);
    std::string vtkfile = "poisson";
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter, gfs, x, Dune::PDELab::vtk::defaultNameScheme());
    vtkwriter.write(vtkfile, Dune::VTK::ascii);


    // Maybe calculate errors for test results...
    using DGF = Dune::PDELab::DiscreteGridFunction<decltype(gfs),decltype(x)>;
    DGF dgf(gfs,x);
    using DifferenceSquaredAdapter_ = Dune::PDELab::DifferenceSquaredAdapter<decltype(func_0000), decltype(dgf)>;
    DifferenceSquaredAdapter_ dsa_(func_0000, dgf);
    // L2 error squared of difference between numerical
    // solution and the interpolation of exact solution
    // for treepath ()
    typename decltype(dsa_)::Traits::RangeType err(0.0);
    Dune::PDELab::integrateGridFunction(dsa_, err, 10);

    if (gv.comm().rank() == 0){
      std::cout << "L2 Error for treepath : " << err << std::endl;
    }
  }
  catch (Dune::Exception& e)
  {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << "Unknown exception thrown: " << e.what() << std::endl;
    return 1;
  }
}
