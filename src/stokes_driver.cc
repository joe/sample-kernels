#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <chrono>
#include <sstream>
#include <sys/types.h>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include "dune/common/parallel/mpihelper.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/common/parametertree.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/grid/io/file/vtk.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/common/partitionviewentityset.hh"
#include "dune/pdelab/common/functionutilities.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/pdelab/gridfunctionspace/subspace.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/sample-kernels/common/matrixfree.hh"
#include "dune/sample-kernels/common/blockstructuredqkfem.hh"
#include "dune/sample-kernels/localoperator/stokes_lop.hh"


#define NBLOCKS 12

int main(int argc, char** argv){
  try
  {
    // Initialize basic stuff...
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    using RangeType = double;

    // Setup grid (view)...
    using Grid = Dune::YaspGrid<2, Dune::EquidistantCoordinates<RangeType, 2>>;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    const auto cellsPerDir = 1;
    std::shared_ptr<Grid> grid(Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, 2>{0,0},
                                                                                 Dune::FieldVector<double, 2>{1,1},
                                                                                 std::array<unsigned int, 2>{cellsPerDir,cellsPerDir}));
    GV gv = grid->leafGridView();

    // Set up finite element maps...
    using Q2_FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, 2 * NBLOCKS>;
    using Q1_FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, NBLOCKS>;
    Q2_FEM q2_fem(gv);
    Q1_FEM q1_fem(gv);

    // Set up grid function spaces...
    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
    using DirichletConstraintsAssember = Dune::PDELab::ConformingDirichletConstraints;
    using Q2_GFS = Dune::PDELab::GridFunctionSpace<GV, Q2_FEM, DirichletConstraintsAssember, VectorBackend>;
    Q2_GFS q2_gfs(gv, q2_fem);
    q2_gfs.name("q2_gfs");

    using Q2_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<Q2_GFS, 2, VectorBackend>;
    Q2_POW2GFS q2_pow2gfs(q2_gfs);
    using namespace Dune::Indices;
    q2_pow2gfs.child(_0).name("q2_pow2gfs_0");
    q2_pow2gfs.child(_1).name("q2_pow2gfs_1");

    using NoConstraintsAssembler = Dune::PDELab::NoConstraints;
    using Q1_GFS = Dune::PDELab::GridFunctionSpace<GV, Q1_FEM, NoConstraintsAssembler, VectorBackend>;
    Q1_GFS q1_gfs_1(gv, q1_fem);
    q1_gfs_1.name("q1_gfs_1");

    using Q2_POW2GFS_Q1_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackend, Dune::PDELab::LexicographicOrderingTag, Q2_POW2GFS, Q1_GFS>;
    Q2_POW2GFS_Q1_GFS q2_pow2gfs_q1_gfs(q2_pow2gfs, q1_gfs_1);
    q2_pow2gfs_q1_gfs.update();

    // Set up constraints container...
    using CC = Q2_POW2GFS_Q1_GFS::ConstraintsContainer<RangeType>::Type;
    CC cc;
    cc.clear();
    auto q2_bctype_lambda = [&](const auto& x){ return (x[0] < 0.99999999 ? 1 : 0.0); };
    auto q2_bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, q2_bctype_lambda);
    Dune::PDELab::PowerConstraintsParameters<decltype(q2_bctype), 2> q2_bctype_pow2bctype(q2_bctype);
    auto q1_bctype_lambda = [&](const auto& x){ return 0.0; };
    auto q1_bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, q1_bctype_lambda);
    Dune::PDELab::CompositeConstraintsParameters<decltype(q2_bctype_pow2bctype), decltype(q1_bctype)> bctype(q2_bctype_pow2bctype, q1_bctype);
    Dune::PDELab::constraints(bctype, q2_pow2gfs_q1_gfs, cc);

    // Set up grid grid operators...
    using LOP_R = LocalOperatorR<Q2_POW2GFS_Q1_GFS, Q2_POW2GFS_Q1_GFS, RangeType, NBLOCKS>;
    using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
    using GO_r = Dune::PDELab::GridOperator<Q2_POW2GFS_Q1_GFS, Q2_POW2GFS_Q1_GFS, LOP_R, MatrixBackend, DF, RangeType, RangeType, CC, CC>;
    LOP_R lop_r(q2_pow2gfs_q1_gfs, q2_pow2gfs_q1_gfs);
    q2_pow2gfs_q1_gfs.update();
    int dofestimate =  4 * q2_pow2gfs_q1_gfs.maxLocalSize();
    MatrixBackend mb(dofestimate);
    GO_r go_r(q2_pow2gfs_q1_gfs, cc, q2_pow2gfs_q1_gfs, cc, lop_r, mb);
    std::cout << "gfs with " << q2_pow2gfs_q1_gfs.size() << " dofs generated  "<< std::endl;
    std::cout << "cc with " << cc.size() << " dofs generated  "<< std::endl;

    // Set up solution vectors...
    using V_R = Dune::PDELab::Backend::Vector<Q2_POW2GFS_Q1_GFS,DF>;
    V_R x_r(q2_pow2gfs_q1_gfs);
    x_r = 0.0;
    auto lambda_0000 = [&](const auto& x){ return (double)(1.0 - x[1]) * 4.0 * x[1]; };
    auto func_0000 = Dune::PDELab::makeGridFunctionFromCallable(gv, lambda_0000);
    auto lambda_0001 = [&](const auto& x){ return (double)0.0; };
    auto func_0001 = Dune::PDELab::makeGridFunctionFromCallable(gv, lambda_0001);
    Dune::PDELab::CompositeGridFunction<decltype(func_0000), decltype(func_0001)> func_0000_func_0001(func_0000, func_0001);
    auto lambda_0002 = [&](const auto& x){ return 0.0; };
    auto func_0002 = Dune::PDELab::makeGridFunctionFromCallable(gv, lambda_0002);
    Dune::PDELab::CompositeGridFunction<decltype(func_0000_func_0001), decltype(func_0002)> func_0000_func_0001_func_0002(func_0000_func_0001, func_0002);
    Dune::PDELab::interpolate(func_0000_func_0001_func_0002, q2_pow2gfs_q1_gfs, x_r);
    auto lambda_0003 = [&](const auto& x){ return (double)8.0 * (1.0 - x[0]); };
    auto func_0003 = Dune::PDELab::makeGridFunctionFromCallable(gv, lambda_0003);

    // Set up (non)linear solvers...
//    using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_GMRES_ILU0;
//    using SLP = Dune::PDELab::StationaryLinearProblemSolver<GO_r, LinearSolver, V_R>;
//    LinearSolver ls;
//    double reduction = 1e-10;
//    SLP slp(go_r, ls, x_r, reduction, 1e-50, 2);
//    slp.apply();
    Dune::PDELab::solveMatrixFreeMINRES(go_r, x_r);

    // Do visualization...
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    Dune::RefinementIntervals subint(NBLOCKS);
    VTKWriter vtkwriter(gv, subint);
    std::string vtkfile = "stokes";
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter, q2_pow2gfs_q1_gfs, x_r, Dune::PDELab::vtk::defaultNameScheme());
    vtkwriter.write(vtkfile, Dune::VTK::ascii);
    Dune::PDELab::GridFunctionSubSpace<Q2_POW2GFS_Q1_GFS, Dune::TypeTree::TreePath<0, 0> > subSpace_0_0(q2_pow2gfs_q1_gfs);
    Dune::PDELab::GridFunctionSubSpace<Q2_POW2GFS_Q1_GFS, Dune::TypeTree::TreePath<0, 1> > subSpace_0_1(q2_pow2gfs_q1_gfs);
    Dune::PDELab::GridFunctionSubSpace<Q2_POW2GFS_Q1_GFS, Dune::TypeTree::TreePath<1> > subSpace_1(q2_pow2gfs_q1_gfs);


    // Maybe calculate errors for test results...
    using Q2_DIRICHLET_GFS_0_0_POW2GFS_0_Q1_GFS_1__0_0_DGF = Dune::PDELab::DiscreteGridFunction<decltype(subSpace_0_0),decltype(x_r)>;
    Q2_DIRICHLET_GFS_0_0_POW2GFS_0_Q1_GFS_1__0_0_DGF q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__0_0_dgf(subSpace_0_0,x_r);
    using DifferenceSquaredAdapter_0_0 = Dune::PDELab::DifferenceSquaredAdapter<decltype(func_0000), decltype(q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__0_0_dgf)>;
    DifferenceSquaredAdapter_0_0 dsa_0_0(func_0000, q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__0_0_dgf);
    Dune::FieldVector<RangeType, 1> l2error(0.0);
    {
      // L2 error squared of difference between numerical
      // solution and the interpolation of exact solution
      // for treepath (0, 0)
      typename decltype(dsa_0_0)::Traits::RangeType err(0.0);
      Dune::PDELab::integrateGridFunction(dsa_0_0, err, 10);

      l2error += err;
      if (gv.comm().rank() == 0){
        std::cout << "L2 Error for treepath 0, 0: " << err << std::endl;
      }
    }
    using Q2_DIRICHLET_GFS_0_0_POW2GFS_0_Q1_GFS_1__0_1_DGF = Dune::PDELab::DiscreteGridFunction<decltype(subSpace_0_1),decltype(x_r)>;
    Q2_DIRICHLET_GFS_0_0_POW2GFS_0_Q1_GFS_1__0_1_DGF q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__0_1_dgf(subSpace_0_1,x_r);
    using DifferenceSquaredAdapter_0_1 = Dune::PDELab::DifferenceSquaredAdapter<decltype(func_0001), decltype(q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__0_1_dgf)>;
    DifferenceSquaredAdapter_0_1 dsa_0_1(func_0001, q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__0_1_dgf);
    {
      // L2 error squared of difference between numerical
      // solution and the interpolation of exact solution
      // for treepath (0, 1)
      typename decltype(dsa_0_1)::Traits::RangeType err(0.0);
      Dune::PDELab::integrateGridFunction(dsa_0_1, err, 10);

      l2error += err;
      if (gv.comm().rank() == 0){
        std::cout << "L2 Error for treepath 0, 1: " << err << std::endl;
      }
    }
    using Q2_DIRICHLET_GFS_0_0_POW2GFS_0_Q1_GFS_1__1_DGF = Dune::PDELab::DiscreteGridFunction<decltype(subSpace_1),decltype(x_r)>;
    Q2_DIRICHLET_GFS_0_0_POW2GFS_0_Q1_GFS_1__1_DGF q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__1_dgf(subSpace_1,x_r);
    using DifferenceSquaredAdapter_1 = Dune::PDELab::DifferenceSquaredAdapter<decltype(func_0003), decltype(q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__1_dgf)>;
    DifferenceSquaredAdapter_1 dsa_1(func_0003, q2_dirichlet_gfs_0_0_pow2gfs_0_q1_gfs_1__1_dgf);
    {
      // L2 error squared of difference between numerical
      // solution and the interpolation of exact solution
      // for treepath (1)
      typename decltype(dsa_1)::Traits::RangeType err(0.0);
      Dune::PDELab::integrateGridFunction(dsa_1, err, 10);

      l2error += err;
      if (gv.comm().rank() == 0){
        std::cout << "L2 Error for treepath 1: " << err << std::endl;
      }
    }
    if (gv.comm().rank() == 0){
      std::cout << "\nl2errorsquared: " << l2error << std::endl << std::endl;
    }

  }
  catch (Dune::Exception& e)
  {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << "Unknown exception thrown: " << e.what() << std::endl;
    return 1;
  }
}